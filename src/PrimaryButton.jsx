import './index.css'

function PrimaryButton(props) {
  return <button className="primary-button">{props.children}</button>
}

export default PrimaryButton