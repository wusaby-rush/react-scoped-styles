import React from 'react'
import ReactDOM from 'react-dom/client'
import PrimaryButton from './PrimaryButton'
import './index.css'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <PrimaryButton> Login </PrimaryButton>
  </React.StrictMode>,
)
